# test smtp form command line with tls

# start conection
openssl s_client -starttls smtp -connect smtp.pangea.org:587
# say hello 
EHLO aritz.org
# start authentication
AUTH LOGIN
# enter base64 encoded username (echo -n '<text>' | openssl base64)
asdfasfdasdf
# enter base64 encoded password (same echo command as previous step)
asdfasfasdf


Ejemplo que se tracea en en el tester online https://www.gmass.co/smtp-test usando telnet smtp.pangea.org 587:

<< 220 mail.pangea.org ESMTP Postfix (Debian/GNU)
>> EHLO [172.31.4.197]
<< 250-mail.pangea.org
<< 250-PIPELINING
<< 250-SIZE 30240000
<< 250-VRFY
<< 250-ETRN
<< 250-STARTTLS
<< 250-ENHANCEDSTATUSCODES
<< 250-8BITMIME
<< 250 DSN
>> STARTTLS
<< 220 2.0.0 Ready to start TLS
>> EHLO [172.31.4.197]
<< 250-mail.pangea.org
<< 250-PIPELINING
<< 250-SIZE 30240000
<< 250-VRFY
<< 250-ETRN
<< 250-AUTH PLAIN LOGIN
<< 250-AUTH=PLAIN LOGIN
<< 250-ENHANCEDSTATUSCODES
<< 250-8BITMIME
<< 250 DSN
>> AUTH PLAIN AGhvbGFfcGFtYXBhbQBicmVpM2ZlOA==
<< 535 5.7.8 Error: authentication failed:
>> AUTH LOGIN
<< 334 VXNlcm5hbWU6
>> aG9sYV9wYW1hcGFt
<< 334 UGFzc3dvcmQ6
>> YnJlaTNmZTg=
<< 535 5.7.8 Error: authentication failed: UGFzc3dvcmQ6
ERROR: AuthenticationInvalidCredentials: 5.7.8 Error: authentication failed: UGFzc3dvcmQ6 
